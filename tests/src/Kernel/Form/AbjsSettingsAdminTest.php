<?php

namespace Drupal\Tests\abjs\Kernel\Form;

use Drupal\abjs\Form\AbjsSettingsAdmin;
use Drupal\Core\Form\FormInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Kernel tests for AbjsSettingsAdmin.
 *
 * @group abjs
 */
class AbjsSettingsAdminTest extends KernelTestBase {

  use StringTranslationTrait;

  /**
   * The Abjs settings form.
   *
   * @var \Drupal\abjs\Form\AbjsSettingsAdmin
   */
  protected $abjsSettingsForm;

  /**
   * The Abjs settings.
   *
   * @var object
   */
  protected $abjsConfig;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'system',
    'user',
    'imce',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->abjsSettingsForm = new AbjsSettingsAdmin(
      $this->container->get('config.factory')
    );
  }

  /**
   * Test formId().
   */
  public function testFormId() {
    $this->assertTrue(is_string($this->abjsSettingsForm->getFormId()));
    $this->assertEquals('abjs_settings_admin', $this->abjsSettingsForm->getFormId());
  }

  /**
   * Test editable config name.
   */
  public function testEditableConfigName() {
    $method = new \ReflectionMethod(AbjsSettingsAdmin::class, 'getEditableConfigNames');
    $method->setAccessible(TRUE);

    $configName = $method->invoke($this->abjsSettingsForm);
    $this->assertEquals(['abjs.settings'], $configName);
  }

  /**
   * Test abjs settings form instance.
   */
  public function testInstanceType() {
    $this->assertInstanceOf(FormInterface::class, $this->abjsSettingsForm);
  }

}
